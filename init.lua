entrance_test = {}
entrance_test.npcs = {}

local function update_sam() end
local function next_available_ID() end

dofile(minetest.get_modpath("entrance_test") .. "/npcs_editor.lua")



arena_lib.register_entrance_type("entrance_test", "sam", {
  name = "Sam",

  editor_settings = {
    name = "Sams",
    icon = "entrancetest_editor_icon.png",
    description = "Link-a-Sam",
    tools = {
      "entrance_test:npc_add",
  	  "entrance_test:npc_remove"
    }
  },

  on_add = function(sender, mod, arena, pos)
    local id = arena_lib.get_arena_by_name(mod, arena.name)
    local player = minetest.get_player_by_name(sender)
    local sam = minetest.add_entity(player:get_pos(), "entrance_test:sam", "mod=" .. mod .. " id=" .. id)
    local sam_lua = sam:get_luaentity()

    sam_lua.arenalib_mod = mod
    sam_lua.arenalib_id = id

    return sam_lua.npc_id
  end,

  on_remove = function(mod, arena)
    local luaentity = entrance_test.npcs[arena.entrance]
    if not luaentity then return end

    local entity = luaentity.entity

    entrance_test.npcs[arena.entrance] = nil
    minetest.load_area(entity:get_pos())
    entity:remove()
  end,

  on_update = function(mod, arena) update_sam(arena) end,

  debug_output = function(entrance) return entrance end
})



minetest.register_entity("entrance_test:sam",{
  initial_properties = {
    visual = "mesh",
    physical = true,
    mesh = "character.b3d",
    textures = { "character.png" },
    collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
    nametag =  "--- no arena linked, hit me to delete me ---",
  },

  npc_id = -1,
  arenalib_mod = "",
  arenalib_id = -1,

  on_punch = function(self, puncher, time_from_last_punch, toolcaps, dir, damage)
    local arenaID = self.arenalib_id

    if arenaID == -1 then
      self.object.remove()
    end

    local mod = self.arenalib_mod
    local arena = arena_lib.mods[mod].arenas[arenaID]
    local p_name = puncher:get_player_name()

    if arena_lib.is_player_in_queue(p_name, mod) and arena_lib.get_queueID_by_player(p_name) == arenaID then
      arena_lib.remove_player_from_queue(p_name)
    else
      arena_lib.join_queue(mod, arena, p_name)
    end

    return 0
  end,

  get_staticdata = function(self)
    return minetest.serialize({
      npc_id = self.npc_id,
      arenalib_mod = self.arenalib_mod,
      arenalib_id = self.arenalib_id
    })
  end,

  on_activate = function(self, staticdata, dtime_s)
    -- se l'ho appena creato
    if staticdata:sub(1, 4) == "mod=" then
      local data = {}
      for k, v in string.gmatch(staticdata, "(%w+)=(%w+)") do
        data[k] = v
      end

      self.npc_id = next_available_ID()
      entrance_test.npcs[self.npc_id] = {entity = self.object, mod = data.mod, id = data.id}
    -- sennò carico le info
    else
      local data = minetest.deserialize(staticdata)

      self.npc_id = data.npc_id
      self.arenalib_mod = data.arenalib_mod
      self.arenalib_id = data.arenalib_id
      entrance_test.npcs[self.npc_id] = {entity = self.object, mod = self.arenalib_mod, id = self.arenalib_id}
      update_sam(arena_lib.mods[self.arenalib_mod].arenas[self.arenalib_id])
    end
  end
})





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function update_sam(arena)
  local p_count = 0
  local t_count = #arena.teams

  for pl, stats in pairs(arena.players) do
    p_count = p_count +1
  end

  local status

  if not arena.enabled then status = "WIP"
  elseif arena.in_queue then status = "Queueing"
  elseif arena.in_celebration then status = "Terminating"
  elseif arena.in_loading then status = "Loading"
  elseif arena.in_game then status = "In progress"
  else status = "Waiting" end

  local sam = entrance_test.npcs[arena.entrance].entity

  sam:set_nametag_attributes({text =
    arena.name ..  " > "
    .. p_count .. "/".. arena.max_players * t_count .. " ||| "
    .. status
  })
end



function next_available_ID(mod_ref)
  local id = 0
  for k, v in ipairs(entrance_test.npcs) do
    id = k
  end
  return id +1
end
