minetest.register_tool("entrance_test:npc_add", {

    description = "Add a Sam",
    inventory_image = "entrancetest_tool_npc_add.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)
      local p_name      = user:get_player_name()
      local mod         = user:get_meta():get_string("arena_lib_editor.mod")
      local arena_name  = user:get_meta():get_string("arena_lib_editor.arena")

      arena_lib.set_entrance(p_name, mod, arena_name, "add", nil)
    end
})



minetest.register_tool("entrance_test:npc_remove", {

    description = "Remove a Sam",
    inventory_image = "entrancetest_tool_npc_remove.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)
      local p_name      = user:get_player_name()
      local mod         = user:get_meta():get_string("arena_lib_editor.mod")
      local arena_name  = user:get_meta():get_string("arena_lib_editor.arena")

      arena_lib.set_entrance(p_name, mod, arena_name, "remove")
    end
})
